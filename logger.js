require('draftlog').into(console);
const fs = require('fs');
const fsPromise = fs.promises;
const CliSpinners = require('cli-spinners');
const Chalk = require('chalk');

class Logger {
  constructor (config) {
    if(config.bottom-bar == true) {
      this.frames = ['(O   )','( O  )','(  O )','(   O)','(  O )','( O  )'];
      this.index = 0;
      this.current = config.current ?? 0;
      this.max = config.max ?? 0;
      this.size = config.size ?? 50;
      this.bottomBar = true;
      this.bottomBarText = config.text ?? '';
      this.lastLine = console.draft();
    }
    else {
      this.bottomBar = false;
    }
  }

  async bottomBarInit () {
    let init = () => {
      this.bottomBarContentUpdate();
    };

    this.bottomBarInterval = setInterval(init,80);
  }

  async bottomBarUpdate(current, max ,text) {
    this.current = current;
    this.max = max;
    this.bottomBarText = text;
    this.bottomBarContentUpdate();
  }

  bottomBarContentUpdate () {
    let frame = this.frames[this.index++ % this.frames.length];
    let ratio = this.current/this.max;
    let processBar = `[${'='.repeat(Math.floor(ratio*this.size))}${' '.repeat(this.size - Math.floor(ratio*this.size))}] | ${Math.round(ratio*100)}% | Step ${this.current}/${this.max}`;
    this.currentBottomBarText = `${processBar} | ${this.bottomBarText} ${frame}`;
    this.lastLine(this.currentBottomBarText);
  }

  async createProcessingBar(current,max,text) {
    let draft = null;
    let size = 50; //Default size is 50
    switch (this.bottomBar) {
      //If bottomBar is true
      case true:
        //Append new processingBar to last line
        this.lastLine(Chalk.grey(await this.calculateProcessingBar(current,max,size,text)));
        draft = this.lastLine;
        //Create new line
        let newLine = console.draft();
        this.lastLine = newLine;
        break;
      //If bottomBar isn't true
      case false:
        //Create new line of processingBar
        draft = console.draft(Chalk.grey(await this.calculateProcessingBar(current,max,size,text)));
        break;
    }

    let logLine = {
      "current": current,
      "max": max,
      "size": size,
      "draft": draft
    };

    return logLine;
  }

  async updateProcessingBar(logLine,current,text) {
    let draft = logLine.draft;
    draft(Chalk.grey(await this.calculateProcessingBar(current,logLine.max,logLine.size,text)));
    logLine.current = current;
    logLine.draft = draft;
    return logLine;
  }

  async calculateProcessingBar (current,max,size,text) {
    let ratio = current/max;
    let processBar = `[${'='.repeat(Math.floor(ratio*size))}${' '.repeat(this.size - Math.floor(ratio*size))}] | ${Math.round(ratio*100)}% | Step ${current}/${max}`;
    return `${processBar} | ${text}`;
  }

  async log (str) {
    this.lastLine(str);
    let tmp = this.lastLine;
    let newLine = console.draft();
    this.lastLine = newLine;
    return tmp;
  }

  async updateLogLineAndSaveToFile (logLine,str,path) {
    logLine(str);
    try {
      fsPromise.appendFile(path,str);
    }
    catch (error) {
      tmp = await log(str);
    }
    return logLine;
  }

  async logAndSaveToFile (str,path) {
    let tmp = null;
    try {
      tmp = await log(str);
      fsPromise.appendFile(path,str);
    }
    catch (error) {
      tmp = await log(str);
    }
    return tmp;
  }


  async end() {
    clearInterval(this.bottomBarInterval);
    this.lastLine(this.currentBottomBarText);
  }
}
module.exports = Logger;
