const AWS = require('aws-sdk');
const ProxyAgent = require('proxy-agent');

class AWSUtilitites {
  constructor (config) {
    this.configuration = {
      "accessKeyId": config.accessKeyId,
      "secretAccessKey": config.secretAccessKey,
      "region": config.region
    };

    //Update proxy if proxy being set in configuration
    if((config.proxy ?? '') != '') {
      AWS.config.update({
        httpOptions: {
          agent: ProxyAgent(config.proxy)
        }
      });
    }
  }

  async s3Upload(inputStream, bucket, s3Key) {
    const s3 = new AWS.S3(this.credential);
    var respond = null;

    const params = {
      Bucket: bucket,
      Key: s3Key,
      Body: inputStream
    };

    try{
      respond = await s3.upload(params).promise();
    }
    catch (error) {
      return error;
    }
    return `File uploaded successfully at ${respond.Location}`;
  }

  async s3Read(bucket, s3Key) {
    const s3 = new AWS.S3(this.credential);
    var data = '';

    const params = {
      Bucket: bucket,
      Key: s3Key
    };

    var tmp = null;          //Default return null
    var timeout = 10;
    try {
      data = await s3.getObject(params).promise();
      tmp = Buffer.from(data.Body).toString();
    }
    catch (error) {
      return error;
    }

    return tmp;
  }
}
module.exports = AWSUtilitites;
