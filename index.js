const DateFormat = require('./dateformat.js');
const JSONUtilities = require('./json_utilities.js');
const Utilities = require('./utilities.js');
const CliSpinners = require('cli-spinners');
const Chalk = require('chalk');
const AWSUtilitites = require('./aws_utilities.js');
const ConfigurationLoader = require('./configuration_loader.js');
const Logger = require('./logger.js');

//Constants
const CONFIGURATION_FILE_PATH = './configurations.json';
const MAIN_THREAD_INITIAL_STEPS = 3;

//Global variables
let configuration = null;
let aws_client = null;
let log = new Logger({ "bottom-bar": true, "current": 0, "max": 100, "text": "Initialing"});
let exitFlag = false;

//Initial function here
async function init () {
  await log.bottomBarInit();
  await log.bottomBarUpdate(1,MAIN_THREAD_INITIAL_STEPS,`Initialing`);
  configuration = await new ConfigurationLoader(CONFIGURATION_FILE_PATH);
  await configuration.reloadConfig();
  if (configuration.loadResult['status'] == 'Failed') {
    log.bottomBarUpdate(1,MAIN_THREAD_INITIAL_STEPS,configuration.loadResult['description']);
    process.exit(1);
  }
  aws_client = new AWSUtilitites(await configuration.getConfiguration());
  //Initial process here

  //Initial process end
}

//Initial check here
async function initCheck () {
  let result = {
    status: true,
    description: ''
  }
  let failures = [];
  if ((configuration ?? '') == '') {
    result['status'] = false;
    failures.push(`No configuration loaded!!!`);
  }
  if ((aws_client ?? '') == '') {
    result['status'] = false;
    failures.push(`AWS is not initialed!!!`);
  }
  //Inital check here

  //Initial check end
  result['description'] = failures.join(',');
  return result;
}

//Sleep function
async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//Flow control initial
async function initFlowControl() {
  process.on('SIGINT',() => { setExitFlag(); });
  exitFlagCheck();
}

//Flow control, ending signal check and run ending process function
async function exitFlagCheck() {
  while (true) {
    await sleep(1000);
    if (exitFlag == true) {
      await log.bottomBarUpdate(3,MAIN_THREAD_INITIAL_STEPS,`End signal set!!! Runing ending process!`);
      //Ending process here


      //End ending process
      await log.bottomBarUpdate(3,MAIN_THREAD_INITIAL_STEPS,`Ended!!!`);
      await log.end();
      process.exit(1);
    }
  }
}

//Function for setting exit flag
function setExitFlag() {
  exitFlag = true;
}

async function detectFileUpload () {
    let processingBar = await log.createProcessingBar(0,2,'Getting file');
    let data = await Utilities.readFile('./test.txt');
    if (data['status'] == 'Success') {
      processingBar = await log.updateProcessingBar(processingBar,1,'Uploading');
      processingBar = await log.updateProcessingBar(processingBar,2,await aws_client.s3Upload(data['data'],'acbs-test-data','test.txt'));
    }
    else {
      processingBar = await log.updateProcessingBar(processingBar,0,data['description']);
    }
}

async function main() {
  //Initial flow controll
  initFlowControl();

  //Initial main components
  await init();

  //Initial checking after initial process
  await log.bottomBarUpdate(2,MAIN_THREAD_INITIAL_STEPS,`Checking`)
  let initCheckResult = await initCheck();
  if(initCheckResult['status'] == false) {
    log.bottomBarUpdate(2,MAIN_THREAD_INITIAL_STEPS,initCheckResult['description']);
    process.exit(1);
  }

  //Update main thread log if inital process been done successfully
  await log.bottomBarUpdate(3,MAIN_THREAD_INITIAL_STEPS,`Runing`);

  //After initial
  detectFileUpload();
}
main();
