const DateFormat = require('./dateformat.js');
const fs = require('fs');
const fsPromise = fs.promises;

function todayFileName() {
  return DateFormat.dateFormat(new Date(), "yyyymmdd");
}

function todayLog() {
  return DateFormat.dateFormat(new Date(), "yyyy-mm-dd HH:mm");
}

function standardizedDateMonthDigit(number) {
  if(number.toString().length == 1) {
    result = `0${number}`;
  }
  else {
    result = number.toString();
  }
  return result;
}

function requestBody(json_request_body) {
  //json_request_body = JSON.parse(json_request_body);
  var request_body = [];

  for (var i in json_request_body) {
  request_body.push(`${encodeURIComponent(i)}=${encodeURIComponent(json_request_body[i])}`);
  }

  request_body = request_body.join('&');

  return request_body;
}

async function readFile(path) {
  let result = {
    status: '',
    description: '',
    data: ''
  };
  try {
    result.data = await fsPromise.readFile(path);
    result.status = 'Success';
  }
  catch (error) {
    result.status = 'Failed';
    result.description = error;
  }
  return result;
}

async function writeFile(path,data) {
  let result = {
    status: '',
    description: ''
  };
  try {
    await fsPromise.writeFile(path,data.toString());
    result.status = 'Success';
    result.description = `Saved success at ${path}`;
  }
  catch (error) {
    result.status = 'Failed';
    result.description = error;
  }
  return result;
}

function dateDiff(date1,date2) {
  let diff = Math.abs(date1.getTime() - date2.getTime());
  return Math.round(diff / (1000 * 3600 * 24));
}

function utiTest() {
  for(var i=1;i<=31;i++) {
    console.log(standardizedDateMonthDigit(i));
    i++;
  }
}

async function readDir() {
  let files = [];
  let dirs = [];
  let dirContents = await FSPromises.readdir(PATH);
  dirContents.forEach((i) => {
    try {
      if (FS.statSync(PATH + '/' + i).isDirectory() == true) {
        dirs.push(i);
      }
      if (FS.statSync(PATH + '/' + i).isFile() == true) {
        files.push(i);
      }
    }
    catch (error) {
      console.log(error);
    }
  });
  return { files: files, directories: dirs };
}

module.exports = {
  requestBody,
  standardizedDateMonthDigit,
  todayFileName,
  todayLog,
  writeFile,
  readFile,
  dateDiff,
  readDir
};
