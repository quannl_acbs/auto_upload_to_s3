const DateFormat = require('./dateformat.js');
const JSONUtilities = require('./json_utilities.js');
const Utilities = require('./utilities.js');

class ConfigurationLoader {
  constructor (path) {
    this.path = path;
  }

  async reloadConfig () {
    this.loadResult = await Utilities.readFile(this.path);
    if (this.loadResult.status == 'Success') {
      this.configuration = JSON.parse(this.loadResult.data);
    }
    await this.checkConfiguration();
  }

  async checkConfiguration () {
    let failures = [];
    if ((this.path ?? '') == '') {
      failures.push('No path for configuration fould!!!');
      this.loadResult.status = 'Failed';
    }
    if ((this.configuration ?? '') == '') {
      failures.push('Configuration load failed - unexpected reason!!!');
      this.loadResult.status = 'Failed';
    }
    if ((this.loadResult.description ?? '') == '') {
      this.loadResult.description = failures.join(',');
    }
    else {
      this.loadResult.description += ',' + failures.join(',');
    }
  }

  async getConfiguration () {
    return this.configuration;
  }
}

module.exports = ConfigurationLoader;
